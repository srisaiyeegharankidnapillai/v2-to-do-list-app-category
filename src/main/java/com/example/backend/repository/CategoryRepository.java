package com.example.backend.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.backend.model.Category;

@Repository
public interface CategoryRepository extends CrudRepository<Category,Long> {

	public List<Category> findByCategoryUserOrderByCategoryIdAsc(Long userId);
}
