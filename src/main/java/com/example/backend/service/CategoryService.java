package com.example.backend.service;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.backend.messaging.CategoryDTO;
import com.example.backend.model.Category;
import com.example.backend.repository.CategoryRepository;

@Service
public class CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;

	
	public List<CategoryDTO> getAllCategoriesForUser(Long userId)
	{
		 List<Category> categories = categoryRepository.findByCategoryUserOrderByCategoryIdAsc(userId);		 
		 return convertToCategoryDTO(categories);
	}
	
	public String AddCategoryForUser(CategoryDTO categoryDTO, Long userId)
	{
		//AppUser appUser = convertToAppUser(appUserDTO);
		categoryDTO.setCategoryUser(userId);
		Category category = convertToCategory(categoryDTO);
		categoryRepository.save(category);
		return "Category Added";
	}

	public CategoryDTO getCategoryForUser(Long userId, Long categoryId) {
		
		List<CategoryDTO>categoriesDTO = getAllCategoriesForUser(userId);
		CategoryDTO tempCategory = null;
		for (CategoryDTO c : categoriesDTO)
		{
			if (c.getCategoryId() == categoryId) tempCategory = c;	
		}
		return tempCategory;
	}

	public String updateCategoryForUser(Long userId, Long categoryId, CategoryDTO categoryDTO) {
		//AppUser user = appUserService.getUser(userId);
		List<CategoryDTO>categoriesDTO = getAllCategoriesForUser(userId);
		for (CategoryDTO c : categoriesDTO)
		{
			if (c.getCategoryId() == categoryId) {				
				categoryDTO.setCategoryId(categoryId);
				categoryDTO.setCategoryUser(userId);
				if (categoryDTO.getCategoryName() == null)
				{
					categoryDTO.setCategoryName(c.getCategoryName());
				}
				if (categoryDTO.getCategoryDescription() == null)
				{
					categoryDTO.setCategoryDescription(c.getCategoryDescription());
				}
				categoryRepository.save(convertToCategory(categoryDTO));
				return "Category Updated";
			}
		}
		return "Category not found for this user to Update";
	}

	public String deleteCategoryForUser(Long userId, Long categoryId) {
		CategoryDTO tempCategoryDTO = getCategoryForUser(userId, categoryId);
		if (tempCategoryDTO != null) {
			categoryRepository.delete(convertToCategory(tempCategoryDTO));
			return "Category deleted";
		}
		return "Category not found for this user to delete";	
	}
	
	//CategoryList to CategoryDTOList
	private List<CategoryDTO> convertToCategoryDTO(List<Category> category) {
		List<CategoryDTO> categoryDTO = new ArrayList<CategoryDTO> ();
		ModelMapper mm = new ModelMapper();
		
		//Need to skip password
		for (Category c :  category)
		{
			categoryDTO.add(mm.map(c, CategoryDTO.class));
		}
		return categoryDTO;
	}
	
//	//Category to CategoryDTO
//	private CategoryDTO convertToCategoryDTO(Category category) {
//		ModelMapper mm = new ModelMapper();
//		CategoryDTO categoryDTO = mm.map(category, CategoryDTO.class);
//		return categoryDTO;
//	}
	
	//CategoryDTO to Category
	private Category convertToCategory(CategoryDTO categoryDTO) {
		ModelMapper mm = new ModelMapper();
		Category category = mm.map(categoryDTO, Category.class);
		return category;
	}
	
}
