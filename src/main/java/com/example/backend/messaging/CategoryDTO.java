package com.example.backend.messaging;

public class CategoryDTO {
	
	private Long categoryId;
	private String categoryName;
	private String categoryDescription;
	private Long categoryUser;
	
	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryDescription() {
		return categoryDescription;
	}

	public void setCategoryDescription(String categoryDescription) {
		this.categoryDescription = categoryDescription;
	}

	public Long getCategoryUser() {
		return categoryUser;
	}

	public void setCategoryUser(Long categoryUser) {
		this.categoryUser = categoryUser;
	}	

}
