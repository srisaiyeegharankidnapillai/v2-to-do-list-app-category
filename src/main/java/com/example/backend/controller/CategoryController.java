package com.example.backend.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.backend.messaging.CategoryDTO;
import com.example.backend.service.CategoryService;

//@CrossOrigin(origins = {"${app.crossorigin.host1}", "${app.crossorigin.host2}" })
@RestController
@RequestMapping("/api-category/")
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;
	

	@RequestMapping("/hi")
	private String getHi() {
		return "Hi Sri - Category is working on Openshift";
	}
	
	@RequestMapping("users/{userId}/categories/{categoryId}")
	private CategoryDTO getCategoryForUser(@PathVariable Long userId, @PathVariable Long categoryId)
	{
		return categoryService.getCategoryForUser(userId, categoryId);
	}
	
	@RequestMapping("users/{userId}/categories")
	private List<CategoryDTO> getAllCategoriesForUser(@PathVariable Long userId)
	{
		return categoryService.getAllCategoriesForUser(userId);
	}
		
	@RequestMapping(value="users/{userId}/categories", method = RequestMethod.POST)
	private String addCategoryForUser(@RequestBody CategoryDTO categoryDTO, @PathVariable Long userId)
	{
		System.out.println("Reached add category for user");
//		AppUserDTO appUserDTO = getUserById(userId);
//		System.out.println(appUserDTO);
		return categoryService.AddCategoryForUser(categoryDTO, userId);
	}
		
	@RequestMapping(value="users/{userId}/categories/{categoryId}", method = RequestMethod.PATCH)
	private String updateCategoryForUser(@RequestBody CategoryDTO categoryDTO, @PathVariable Long userId, @PathVariable Long categoryId)
	{
		//AppUserDTO appUserDTO = getUserById(userId);
		return categoryService.updateCategoryForUser(userId, categoryId, categoryDTO);
	}
	
	@RequestMapping(value="users/{userId}/categories/{categoryId}", method = RequestMethod.DELETE)
	private String deleteCategoryForUser(@PathVariable Long userId, @PathVariable Long categoryId)
	{
		return categoryService.deleteCategoryForUser(userId, categoryId);
	}
	
	
}
